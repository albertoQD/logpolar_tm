#include <iostream>
#include <string>
#include "templatedetector.h"

int main(int argc, char **argv) {
    
    TemplateDetector * detector = new TemplateDetector("../img4/template.jpg");
    cv::Mat res = detector->detectIn("../img4/source3_.jpg");

//     cv::normalize(res, res, 0.0, 1.0, cv::NORM_MINMAX);
//     cv::imshow("result", res);
    
    cv::waitKey(0);
    
    return 0;
}

