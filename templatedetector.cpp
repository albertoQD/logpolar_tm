#include "templatedetector.h"
#include <cmath>
#define MK_VAL 10


TemplateDetector::TemplateDetector(const std::string &template_fn)
{
    this->template_ = cv::imread(template_fn, CV_LOAD_IMAGE_GRAYSCALE );
    
    if (this->template_.empty())
    {
        std::cerr << "[ERROR] The template image could not be loaded!" << std::endl;
    }
    
    this->template_.convertTo(this->template_, CV_64F);
}

TemplateDetector::~TemplateDetector()
{

}

cv::Mat TemplateDetector::detectIn(const std::string &input_fn)
{   
    return this->detectIn(cv::imread(input_fn, CV_LOAD_IMAGE_GRAYSCALE));
}

cv::Mat TemplateDetector::detectIn(const cv::Mat &img)
{
    if (img.empty())
    {
        std::cerr << "[ERROR] The input image could not be loaded!" << std::endl;
    }
    cv::Mat input = img;
    input.convertTo(input, CV_64F);
    input = this->squareImage(input);
    
    cv::Mat tpl_;
    if (this->template_.size() != input.size())
    {
        this->template_ = this->adaptTemplate(input.size(), false);
    }
    tpl_ = this->adaptTemplate(input.size(), true);

    int N = input.rows;
    int M = N - MK_VAL, K = N - MK_VAL;

    // Normalized Cross Correlation with log-polar transformation
    cv::Point maxLoc = this->spomf(input, tpl_, true);
//     cv::Point maxLoc = this->norm_cc(input, tpl_, true);
    
    double ang = ((maxLoc.x - ((N)/2.0)) / (M-1.0)) - 1.0;
//     ang = (std::exp(ang) * 180.0)/CV_PI;
    double scale = 0.0;
    
    if(maxLoc.y < M/2)
    {
        scale = 1.0/std::pow((M-1.0), ((maxLoc.y)/(M-1.0)));
        std::cout << " * Scale before: " << scale << std::endl;
    } else 
    {
        scale = std::pow((M-1.0), ((M-maxLoc.y)/(M-1.0)));
        std::cout << " + Scale before: " << scale << std::endl;
    }
    
    // ------------------------------
    std::cout << "MaxLoc: " << maxLoc << std::endl;
    std::cout << "Angle: " << -ang << std::endl;
    std::cout << "Scale: " << scale << std::endl;
    // ------------------------------
    
    tpl_ = this->adaptTemplate(input.size(), false);
    
//     this->show_normalized(tpl_, "tpl-before");
    cv::Mat trans = cv::getRotationMatrix2D( cv::Point2f(tpl_.cols/2,tpl_.rows/2), -ang, scale );
    cv::warpAffine( tpl_, tpl_, trans, tpl_.size() );
//     this->show_normalized(tpl_, "tpl-after");
    
    maxLoc = this->spomf(input, tpl_, false);
    
    cv::Mat tpl_tmp;
    // Now Get the minarearect
    tpl_.convertTo(tpl_tmp, CV_8UC1);
    cv::threshold( tpl_tmp, tpl_tmp, 0.0, 1.0, cv::THRESH_BINARY );
    std::vector<cv::Point> points;
    cv::Mat_<uchar>::iterator it = tpl_tmp.begin<uchar>();
    cv::Mat_<uchar>::iterator end = tpl_tmp.end<uchar>();
    for (; it != end; ++it) if (*it) points.push_back(it.pos());

    // Compute minimal bounding box
    cv::RotatedRect box = cv::minAreaRect(cv::Mat(points));    
    cv::Rect roi;
    roi.width = box.size.width;
    roi.height = box.size.height;
    roi.x = std::max(0, int(round(maxLoc.x-(roi.width/2)))); 
    roi.y = std::max(0, int(round(maxLoc.y-(roi.height/2))));
    
    cv::Mat tmp = cv::Mat::zeros(2,3, CV_64F);
    tmp.at<double>(0,0) = 1.0;
    tmp.at<double>(0,1) = 0.0;
    tmp.at<double>(0,2) = maxLoc.x - roi.x;
    tmp.at<double>(1,0) = 0.0;
    tmp.at<double>(1,1) = 1.0;
    tmp.at<double>(1,2) = maxLoc.y - std::abs(roi.height);
    
    cv::warpAffine(tpl_, tpl_, tmp, tpl_.size());
    
    cv::rectangle(input, roi, cv::Scalar(0, 0, 0));

    std::cout << "MaxLoc: " << maxLoc << std::endl;
//     this->show_normalized(cc, "correlation");
//     this->show_normalized(tpl_, "template-affined");
    
    return input;
}

cv::Point2d TemplateDetector::spomf(const cv::Mat &img, const cv::Mat &tpl, bool logPolar_)
{
    cv::Mat img_, tpl_, input_ft, target_ft, input_mag, target_mag,
            input_phas, target_phas, term1, term2, pc;
    img.copyTo(img_); tpl.copyTo(tpl_);
            
    if (logPolar_)
    {
        img_ = this->logPolar(img_);
        tpl_ = this->logPolar(tpl_);
    }
    
    cv::dft(img_, input_ft, cv::DFT_COMPLEX_OUTPUT);
    cv::dft(tpl_, target_ft, cv::DFT_COMPLEX_OUTPUT);
    
    cv::Mat planes[2];
    cv::split(input_ft, planes);
    cv::magnitude(planes[0], planes[1], input_mag);
    cv::split(target_ft, planes);
    cv::magnitude(planes[0], planes[1], target_mag);
    
    planes[0] = input_mag; planes[1] = cv::Mat::ones(input_mag.size(), CV_64F);
    cv::merge(planes, 2, input_mag);
    planes[0] = target_mag; planes[1] = cv::Mat::ones(target_mag.size(), CV_64F);
    cv::merge(planes, 2, target_mag);
    
    cv::divide(input_ft, input_mag, term1);
    cv::divide(target_ft, target_mag, term2);

    cv::mulSpectrums(term2, term1, pc, 0, true);
    cv::dft(pc, pc, cv::DFT_REAL_OUTPUT | cv::DFT_INVERSE);
    
    cv::Point maxLoc = this->maxLocation(pc);
    
    cv::circle(pc, maxLoc, 5, cv::Scalar(255,0,0), -1);
    this->show_normalized(pc, "phase-correlation");
    
    std::cout << "inner-maxloc: " << maxLoc << std::endl;
    std::cout << "pc.size(): " << pc.size() << std::endl;
    
    return maxLoc;
}

cv::Point2d TemplateDetector::norm_cc(const cv::Mat &img, const cv::Mat &tpl, bool logPolar_)
{
    cv::Mat img_, tpl_, input_ft, target_ft, input_mag, target_mag;
    img.copyTo(img_); tpl.copyTo(tpl_);    
    
    cv::dft(img_, input_ft, cv::DFT_COMPLEX_OUTPUT);
    cv::dft(tpl_, target_ft, cv::DFT_COMPLEX_OUTPUT);
//     input_ft = this->fftshift(input_ft);
//     target_ft = this->fftshift(target_ft);
    
    cv::Mat planes[2];
    cv::split(input_ft, planes);
    cv::magnitude(planes[0], planes[1], input_mag);
    cv::split(target_ft, planes);
    cv::magnitude(planes[0], planes[1], target_mag);
    
    if (logPolar_)
    {
        input_mag = logPolar(input_mag);
        target_mag = logPolar(target_mag);
    }
    
    cv::Scalar mean, stddev;
    int N = (img_.rows*img_.cols);
    
    cv::meanStdDev(input_mag, mean, stddev);
    input_mag -= mean.val[0];
    input_mag /= stddev.val[0];
    input_mag /= N;
    cv::meanStdDev(target_mag, mean, stddev);
    target_mag -= mean.val[0];
    target_mag /= stddev.val[0];
    target_mag /= N;
    
    cv::Mat cc;
    cv::mulSpectrums(target_mag, input_mag, cc, 0, true);
    
    cv::dft(cc, cc, cv::DFT_REAL_OUTPUT | cv::DFT_INVERSE);
//     cc = this->fftshift(cc);
    
    this->show_normalized(cc, "correlation", true);
    
    cv::Point maxLoc = this->maxLocation(cc);
//     cv::minMaxLoc(cc, NULL, NULL, NULL, &maxLoc);
    
    return maxLoc;
}

cv::Mat TemplateDetector::adaptTemplate(const cv::Size &target, bool shifting)
{
    cv::Mat tmp = cv::Mat::zeros(target, CV_64F);
    if (this->template_.size() != target)
    {
        cv::Rect middle;
        middle.x = round(tmp.cols/2 - this->template_.cols/2);
        middle.y = round(tmp.rows/2 - this->template_.rows/2);
        middle.width = this->template_.cols;
        middle.height = this->template_.rows;
        this->template_.copyTo(tmp(middle));
    }
    else
    {
        this->template_.copyTo(tmp);
    }
    
    if (shifting) tmp = this->fftshift(tmp);
    
    return tmp;
}

cv::Mat TemplateDetector::fftshift(const cv::Mat &img)
{
    cv::Mat res = cv::Mat::zeros(img.size(), CV_64F);
    img.copyTo(res);
    
    int cx = res.cols/2;
    int cy = res.rows/2;

    cv::Mat q0(res, cv::Rect(0, 0, cx, cy));   // Top-Left - Create a ROI per quadrant
    cv::Mat q1(res, cv::Rect(cx, 0, cx, cy));  // Top-Right
    cv::Mat q2(res, cv::Rect(0, cy, cx, cy));  // Bottom-Left
    cv::Mat q3(res, cv::Rect(cx, cy, cx, cy)); // Bottom-Right

    cv::Mat tmp;                               // swap quadrants (Top-Left with Bottom-Right)
    q0.copyTo(tmp);
    q3.copyTo(q0);
    tmp.copyTo(q3);

    q1.copyTo(tmp);                            // swap quadrant (Top-Right with Bottom-Left)
    q2.copyTo(q1);
    tmp.copyTo(q2);
    
    return res;
}

cv::Mat TemplateDetector::squareImage(const cv::Mat &img)
{
    cv::Mat padded = cv::Mat::zeros(std::max(img.rows, img.cols), 
                                    std::max(img.rows, img.cols), CV_64F);    
    int diff = round(std::abs((img.rows - img.cols))/2);
    
    if (img.rows > img.cols)
    { // pad by cols
        img.copyTo(padded(cv::Rect(diff, 0, img.cols, img.rows)));
    } else if (img.cols > img.rows)
    { // pad by rows
        img.copyTo(padded(cv::Rect(0, diff, img.cols, img.rows)));
    } else 
    { // copy the same
        img.copyTo(padded);
    }
    
    return padded;
    
}

cv::Mat TemplateDetector::logPolar(const cv::Mat &img)
{
    double N = img.rows;
    double M = N - MK_VAL, K = N - MK_VAL;
    
    cv::Mat in;
    img.convertTo(in, CV_64F);
    
    cv::Mat polar = cv::Mat::zeros(M, K, CV_64F);
       
    for (double m=0.0; m<M; m+=1.0)
    {
        for(double k=0.0; k<K; k+=1.0)
        {   
            double u = ( ((N/2.0)-1.0)/(M-1.0) ) * std::pow(M-1.0, m/(M-1.0)) * cos(CV_PI * (k / K)) + (N/2.0);
            double v = ( ((N/2.0)-1.0)/(M-1.0) ) * std::pow(M-1.0, m/(M-1.0)) * sin(CV_PI * (k / K)) + (N/2.0);
            
            int u_ = int(round(u)), v_ = int(round(v)), m_ = int(round(m)), k_ = int(round(k));
            
            polar.at<double>(m_, k_) = in.at<double>(u_-1, v_-1);
            
        }
    }
    
    return polar;
}

cv::Point2d TemplateDetector::maxLocation(const cv::Mat &img)
{ 
    /* minMaxLoc of opencv return the first in the rows and I need the first in cols */
   
    double max_= -(1L << (sizeof(double)*8)-1);
    cv::Point2d loc_(0, 0);
    
    
    for (int i = 0; i<img.rows; i+=1)
    {
        for (int j = 0; j<img.cols; j+=1)
        {
            if ((img.at<double>(i, j) > max_) || (img.at<double>(i, j) == max_ && j < loc_.x))
            {
                loc_.x = j; loc_.y = i;
                max_ = img.at<double>(i, j);
            }
        }
    }
    
    return loc_;
}

void TemplateDetector::show_normalized(const cv::Mat &im, const std::string &win_name, bool apply_log)
{
    cv::Mat tmp; 
    im.copyTo(tmp);
    if (apply_log)
    {
        tmp += cv::Scalar(1);
        cv::log(tmp, tmp);
    }
    cv::normalize(tmp, tmp, 0.0, 1.0, cv::NORM_MINMAX);
    cv::imshow(win_name, tmp);
}





