#ifndef TEMPLATEDETECTOR_H
#define TEMPLATEDETECTOR_H

#include <iostream>
#include <string>
#include "opencv2/core/core.hpp"
#include <opencv2/imgproc/imgproc_c.h>
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#define DEBUG_

class TemplateDetector
{
public:
    TemplateDetector(const std::string &);
    ~TemplateDetector();
    
    cv::Mat detectIn(const std::string &);
    cv::Mat detectIn(const cv::Mat &);
    
private:
    cv::Mat fftshift(const cv::Mat &);
    cv::Mat squareImage(const cv::Mat &);
    cv::Mat logPolar(const cv::Mat &);
    cv::Mat adaptTemplate(const cv::Size &target, bool shifting);
    cv::Point2d maxLocation(const cv::Mat &);
    cv::Point2d norm_cc(const cv::Mat&, const cv::Mat&, bool);
    cv::Point2d spomf(const cv::Mat&, const cv::Mat&, bool);
    void show_normalized(const cv::Mat &im, const std::string &win_name, bool apply_log=false);
        
    cv::Mat template_;
};

#endif // TEMPLATEDETECTOR_H
